# Homework 2 - Sergio Alberti VR408286

La mappa creata rappresenta la zona dei distributori automatici di Ca' Vignal 2 (Universit� degli Studi di Verona). 

Contenuto dei file/directory:

- *vending_mac_scaled.world*: modello completo del mondo, creato in Gazebo

- *vending_area_models*: directory contenente tutti i modelli degli oggetti utilizzati per creare il mondo

- *turtlebot3_vending_scaled.launch*: launch Gazebo con la mappa creata e il modello Turtlebot3 Waffle al suo interno

- *slam_generated_maps*: contiene la mappa del mondo generata con guida teleoperata dal Turtlebot tramite SLAM

## Istruzioni setup

I seguenti comandi richiedono che l'ambiente di lavoro (directory e pacchetti) sia quello riportato dalle varie guide relative al turtlebot e a ROS. (riferimenti: [Turtlebot3 PC setup](http://emanual.robotis.com/docs/en/platform/turtlebot3/pc_setup/) e [Turtlebot3 Gazebo Package](http://wiki.ros.org/turtlebot_gazebo))

1. Clonare il repository nella propria home   
    `git clone https://sergioalberti@bitbucket.org/sergioalberti/vr408286_homework2.git`

2. Spostarsi nella cartella clonata    
    `cd vr408286_homework2`

3. Eseguire lo script *setup.sh*    
    `./setup.sh`

## Esecuzione

E' possibile avviare Gazebo con la mappa creata e il modello del Turtlebot gi� presente attraverso il comando:    
`roslaunch turtlebot3_gazebo turtlebot3_vending_scaled.launch`

## Video guida autonoma

Per la generazione del video richiesto � stato utilizzato il materiale descritto nelle precedenti sezioni. Il video � scaricabile a [questo indirizzo](https://www.dropbox.com/s/eihygpmk3dw8qld/VR408286_homework2.mp4?dl=0).
