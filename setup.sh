#!/bin/bash

export TURTLEBOT3_MODEL=waffle

cp vending_mac_scaled.world ~/catkin_ws/src/turtlebot3_simulations/turtlebot3_gazebo/worlds
cp turtlebot3_vending_scaled.launch ~/catkin_ws/src/turtlebot3_simulations/turtlebot3_gazebo/launch
cp -r vending_area_models ~/catkin_ws/src/turtlebot3_simulations/turtlebot3_gazebo/models

echo SETUP DONE

echo
echo To launch vending_scaled world with turtlebot:
echo roslaunch turtlebot3_gazebo turtlebot3_vending_scaled.launch

